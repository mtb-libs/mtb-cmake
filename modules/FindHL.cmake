# ==========
#
# Copyright (c) 2019, Mel Massadian.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
#     * Neither the name of Dan Bethell nor the names of any
#       other contributors to this software may be used to endorse or
#       promote products derived from this software without specific prior
#       written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#==========
#
# Variables defined by this module:
#   HL_FOUND    
#   HL_INCLUDE_DIR
#   HL_COMPILE_FLAGS
#   HL_LIBRARIES
#   HL_LIBRARY_DIR
#
# Usage: 
#   FIND_PACKAGE( HL )
#   FIND_PACKAGE( HL REQUIRED )
#
# Note:
# You can tell the module where Hashlink is installed by setting
# the HL_INSTALL_PATH 
# 
# E.g. 
#   SET( HL_INSTALL_PATH "/usr/local/hl" )
#   FIND_PACKAGE( HL REQUIRED )
#
#==========
message("Looking for Hashlink.")
# our includes
FIND_PATH( HL_INCLUDE_DIR hl.h
  ${HL_INSTALL_PATH}/src
  )

  message("IP:" ${HL_INSTALL_PATH})

# our compilation flags
# SET( 3Delight_COMPILE_FLAGS "-DDELIGHT" )

# our library itself
FIND_LIBRARY( HL_LIBRARIES libhl.dylib
  ${HL_INSTALL_PATH}/build/bin
  )
include_directories(
    ${HL_INSTALL_PATH}/src
    ${HL_INSTALL_PATH}/include/pcre
)
# our library path
GET_FILENAME_COMPONENT( HL_LIBRARY_DIR ${HL_LIBRARIES} PATH )

# did we find everything?
INCLUDE( FindPackageHandleStandardArgs )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( "_HL" DEFAULT_MSG
  HL_INCLUDE_DIR
  # HL_COMPILE_FLAGS
  HL_LIBRARIES
  HL_LIBRARY_DIR
  )
