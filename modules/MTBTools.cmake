
include(Colors)

function(GetProp prop propname target)

  get_property(propval TARGET ${target} PROPERTY ${propname} SET)
  if (propval)
    get_target_property(propval ${target} ${propname})
    
    STRING(REGEX REPLACE ";" " | " propval "${propval}")
    set(${prop} ${propval} PARENT_SCOPE)
  endif()

  
endfunction(GetProp propname target)


macro(TargetInfo target)
  if(NOT TARGET ${target})
    message("There is no target named '${target}'")
    return()
  endif()
  
  # # Get all propreties that cmake supports
  # execute_process(COMMAND cmake --help-property-list OUTPUT_VARIABLE CMAKE_PROPERTY_LIST)
  # # Convert command output into a CMake list
  # STRING(REGEX REPLACE ";" "\\\\;" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")
  # STRING(REGEX REPLACE "\n" ";" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")

  # foreach (prop ${CMAKE_PROPERTY_LIST})
  # string(REPLACE "<CONFIG>" "${CMAKE_BUILD_TYPE}" prop ${prop})
  # if(prop STREQUAL "LOCATION" OR prop MATCHES "^LOCATION_" OR prop MATCHES "_LOCATION$")
  #       continue()
  #   endif()
  #       # message ("Checking ${prop}")
  #       get_property(propval TARGET ${target} PROPERTY ${prop} SET)
  #       if (propval)
  #           get_target_property(propval ${target} ${prop})
  #           message ("${tgt} ${prop} = ${propval}")
  #       endif()
  #   endforeach(prop)

  message("${BoldRed}TargetInfo${ColourReset}:")
  message("\t${BoldYellow}Name:${ColourReset} ${PROJECT_NAME}")
  message("\t${BoldYellow}Prefix Path:${ColourReset} ${CMAKE_MODULE_PATH}")
  message("\t${BoldYellow}Module Path:${ColourReset} ${CMAKE_PREFIX_PATH}")
  get_filename_component(C_COMP ${CMAKE_C_COMPILER} NAME)
  message("\t${BoldYellow}C Compiler:${ColourReset} ${C_COMP}")
  get_filename_component(Cpp_COMP ${CMAKE_CXX_COMPILER} NAME)
  message("\t${BoldYellow}C++ Compiler:${ColourReset} ${Cpp_COMP}")

  set( ALL LINK_LIBRARIES BINARY_DIR  CXX_STANDARD  CXX_STANDARD_REQUIRED SOURCES SOURCE_DIR SUFFIX TYPE)
  foreach (prop ${ALL})
    GetProp(propout ${prop} ${target})
    message("\t${BoldYellow}${prop}:${ColourReset} ${propout}")
  endforeach(prop)
endmacro(TargetInfo)