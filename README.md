# MTB CMake

Various modules for CMake. Licenses kept where due.

This is how I use it:

```cmake
include(FetchContent)

FetchContent_Declare(
  mtb-cmake
  GIT_REPOSITORY git@gitlab.com:mtb-libs/mtb-cmake.git
)

FetchContent_GetProperties(mtb-cmake)

if(NOT mtb-cmake_POPULATED)
        FetchContent_Populate(mtb-cmake)
        list(APPEND CMAKE_MODULE_PATH ${mtb-cmake_SOURCE_DIR}/modules)
        include(SetupMTB) # Colors, Uninstall...
        message(STATUS "> Loaded MTB CMake modules from ${mtb-cmake_SOURCE_DIR}")
endif()
```